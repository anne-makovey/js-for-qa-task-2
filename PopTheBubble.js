const chai = require('chai');

describe('Pop THe Bubble game', () => {
    it ('should pop all after 5s', () => {
    browser.url('https://task1-bvckdxdkxw.now.sh/');
    browser.pause(5000)
    //setTimeout(function() {
    const bubbles = $$('.bubble');
    bubblesQuantity = bubbles.length;
    bubbles.forEach(function(bubble) {
        bubble.click()
    });

    chai.expect(parseInt($('#score').getProperty('innerText'))).to.equal(bubblesQuantity);
//}, 5000);
    });
    //Failed because of "Timeout of 60000ms exceeded."
    it ('should pop all forever', () => { 
        let score = parseInt($('#score').getProperty('innerText'));
        while(true) {
            $('.bubble').waitForDisplayed(5000);
            $('.bubble').click();
            let newScore = parseInt($('#score').getProperty('innerText'));
            chai.expect(newScore).to.equal(score + 1);
            score = newScore;
        }
    });
});

