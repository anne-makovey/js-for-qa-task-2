function sort(a, b) {
    let item1 = a.replace(/\d/g, '').toLowerCase();
    let item2 = b.replace(/\d/g, '').toLowerCase();
    return item1.localeCompare(item2);
};
function superSort(value) {
    let array = [];
    array = value.trim().split(' ');
    array.sort(sort);
    let stringSorted = array.join(' ');
    return stringSorted;
}