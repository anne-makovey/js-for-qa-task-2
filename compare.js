
function compareDate(date1, date2) {
    date1Convert = dateToComparable(date1); console.log(date1Convert);
    date2Convert = dateToComparable(date2); console.log(date2Convert);
    
    return (date1Convert === null || date2Convert === null) ? false : date1Convert - date2Convert === 0;
};
function dateToComparable(dateRaw) {
    let day = 0, month = 0, year = 0;
    let dateArray = [];
    dateArray = dateRaw.trim().split(/[:/\-,.]/g);
    for (i=0; i<dateArray.length; i++ ){
        if (dateArray[i].length === 4) {
            year = dateArray[i];
        }
        else if (dateArray[i] <=12 && month === 0) {
            month = dateArray[i];
        }
        else {
            day = dateArray[i];
        }
    };
    if (month === 0) {return null};
    return new Date(year, month-1, day);
}